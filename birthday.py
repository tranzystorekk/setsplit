#!/usr/bin/env python

import argparse
import sys
import os
import time
import random
import math

from dirutils import *

class Parsed:
    def __init__(self):
        self.mainset = list()
        self.subsets = list()
        self.probability = 50

class Solution:
    def __init__(self):
        self.found = False

        self.subA = list()
        self.subB = list()

        self.sample_size = 0
        self.iteration = 0
        self.scanned = 0.0
        self.sample_code = 0

class Result:
    def __init__(self):
        self.solution = Solution()
        self.time = 0.0

# get cmdline args
def parse_arguments():
    parser = argparse.ArgumentParser(description='Birthday approximation solver for the Set Splitting problem')
    parser.add_argument('--prob',
                        metavar='PERCENTAGE',
                        help='percent probability of guessing a solution (default: 50)',
                        default=[50],
                        type=int,
                        nargs=1)
    parser.add_argument('-f', '--file',
                        metavar='PATH',
                        help='if specified - read input from file in PATH, otherwise from STDIN',
                        nargs=1)
    parser.add_argument('-o', '--out',
                        metavar='PATH',
                        help='if specified - write a report file to PATH, otherwise to report.txt',
                        default=['report.txt'],
                        nargs=1)

    return parser.parse_args()

# parse input from file or stdin
def parse_input(filename=None, prob=50, reportpath='report.txt'):
    if filename is not None:
        inp = open(filename)
    else:
        inp = sys.stdin

    ret = Parsed()
    ret.probability = prob

    inp.readline()
    ret.mainset = list(map(int, inp.readline().split()))

    if len(ret.mainset) < 3:
        print('No solution exists for given case: too few elements in set')
        gen_report(reportpath, Solution(), 0.0)
        sys.exit()

    ret.subsets = list()
    i = 0
    for line in inp:
        if i % 2 == 1:
            templist = list(map(int, line.split()))

            if len(templist) < 2:
                print('No solution exists for given case: too few elements in subset')
                gen_report(reportpath, Solution(), 0.0)
                sys.exit()

            ret.subsets.append(templist)
        i += 1

    inp.close()

    return ret

# solve the problem from parsed data
def solve(parsed):
    n = len(parsed.mainset)
    k = len(parsed.subsets)

    if n > 64:
        print('Error: 64bit search mask insufficient for set size. Exiting.')
        return Solution()

    # total size of search space
    search_size = (1 << n-1) - 1

    # estimated total number of solutions in search space
    nsols = max(round(search_size * 0.01/k), 1)

    #sample_size = math.ceil(math.log(1 - parsed.probability / 100, 1 - nsols/search_size))
    sample_size = math.floor(math.log(1 - parsed.probability / 100, 1 - nsols/search_size))
    sample_size = min(sample_size, search_size)
    if sample_size > (1 << 24):
        print('Sample size for specified probability too large. Aborting.')
        return Solution()

    masklist = random.sample(range(1, 1 << (n-1)), sample_size)

    # WARNING: integer field width limits main set size to 64 (32)
    skip = False
    sol = Solution()
    sol.sample_size = sample_size
    iteration = 1
    for bitarr in masklist:
        # generate partitions by scanning a 2^(n-1)-field bit array
        el = 1
        partA = list()
        partB = [0]

        for pos in range(n-1):
            if bitarr & (1 << pos):
                partA.append(el)
            else:
                partB.append(el)
            el += 1

        subA = [ parsed.mainset[pos] for pos in partA ]
        subB = [ parsed.mainset[pos] for pos in partB ]

        for s in parsed.subsets:
            tmp = set(s)
            if set(subA) >= tmp or set(subB) >= tmp:
                skip = True
                break

        if not skip:
            sol.subA = subA
            sol.subB = subB
            sol.found = True
            #sol.sample_size = sample_size
            sol.iteration = iteration
            sol.scanned = 100 * iteration / sample_size
            sol.sample_code = bitarr
            break

        skip = False
        iteration += 1

    return sol

# measure execution time of a function
def measure(fn, data):
    res = Result()

    start = time.clock()
    res.solution = fn(data)
    end = time.clock()

    res.time = end - start

    return res

# generate a report and write it to [path]
def gen_report(path, solution, time):
    if os.path.dirname(path):
        mkdir_p(os.path.dirname(path))

    outfile = open(path, 'w')

    if solution.found:
        outfile.write('Status: OK\n')
    else:
        outfile.write('Status: ERR\n')

    outfile.write('Execution time: {:f}\n'.format(time))
    outfile.write('Sample size: {}\n'.format(solution.sample_size))
    outfile.write('Iteration: {}\n'.format(solution.iteration))
    outfile.write('Percent scanned: {:f}\n'.format(solution.scanned))
    outfile.write('Solution sample code: {:#x}\n'.format(solution.sample_code))

    outfile.close()

# main program
if __name__ == "__main__":
    args = parse_arguments()

    if args.file is not None:
        data = parse_input(args.file[0], prob=args.prob[0])
    else:
        data = parse_input(prob=args.prob[0], reportpath=args.out[0])

    result = measure(solve, data)

    if result.solution.found:
        print(*result.solution.subA)
        print(*result.solution.subB)

    else:
        print("No solution found for given case")

    gen_report(args.out[0], result.solution, result.time)
